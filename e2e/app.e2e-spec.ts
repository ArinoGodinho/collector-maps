import { CollectorMapsPage } from './app.po';

describe('collector-maps App', () => {
  let page: CollectorMapsPage;

  beforeEach(() => {
    page = new CollectorMapsPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
