import { Component, ElementRef, OnInit, NgZone, ViewChild } from '@angular/core';
import { } from '@types/googlemaps';
import { FormControl } from '@angular/forms';
import { MapsAPILoader } from 'angular2-google-maps/core';
import { MaterializeAction } from "angular2-materialize";

declare var Materialize: any;

@Component({
  selector: 'tag-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css']
})
export class MapsComponent implements OnInit {
  // Nivel zoom
  zoom: number = 13;
  // Posicao inicial
  lat: number = -27.586058;
  lng: number = -48.503237;
  // Mascara para a hora
  public mask = [/[1-9]/, /\d/, ':', /\d/, /\d/, ':', /\d/, /\d/]

  // Elemento de ligacao com a variavel do template html 
  @ViewChild('searchOrigin')
  public searchElementRefOrigin: ElementRef;
  @ViewChild('searchDestination')
  public searchElementRefDestination: ElementRef;
  // Variavel do Form
  public searchControl: FormControl;

  constructor(
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone
  ) { }

  ngOnInit() {
    this.searchControl = new FormControl();
    this.setCurrentPosition();
    this.getAddress(this.searchElementRefOrigin); 
    console.log(this.searchElementRefOrigin);
    this.getAddress(this.searchElementRefDestination);
  }

  updateTextFields() {
    Materialize.updateTextFields();
    console.log("updateTextFields called");
  }

  private setCurrentPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
        this.zoom = 13;
      });
    }
  }

  private getAddress(elementRef: ElementRef) {
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(elementRef.nativeElement, {
        types: ['address']
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          this.lat = place.geometry.location.lat();
          this.lng = place.geometry.location.lng();
          this.zoom = 13;
        });
      });
    });
  }

}
